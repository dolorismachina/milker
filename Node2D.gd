extends Node2D


var gestures = {}

var udders = []

var sounds = []


var cow_hp = 10

func _ready():

	udders.append(get_node("UdderLeft"))
	udders.append(get_node("UdderRight"))

	sounds.append("sound1")
	sounds.append("sound2")

	set_process(true)
	set_process_input(true)



func _process(delta):

	var acc = Input.get_accelerometer()
	var logs = str("X: ", acc.x, "\n", "Y: ", acc.y, "\n", "Z: ", acc.z)

	update()


func _input(event):

	var label = get_node("Label")


	if (event.type == InputEvent.SCREEN_TOUCH) and event.pressed:

		var udder = udder_touched(event.pos)

		if udder != null:

			var gesture = Gesture.new()
			gesture.start = event.pos
			gesture.end = event.pos
			gesture.index = event.index
			gestures[gesture] = udder

			randomize()
			var rnd = randi() % 2
			get_node("SamplePlayer2D").play(sounds[rnd])

	elif event.type == InputEvent.SCREEN_DRAG:

		on_screen_drag(event)

	elif event.type == InputEvent.SCREEN_TOUCH and not event.pressed: # Touch released.

		on_touch_released(event.index)


func udder_touched(pos):

	for i in udders:

		var tex = i.get_texture()
		var hor_in = pos.x > i.get_pos().x - (tex.get_width() / 2 + 50) and pos.x < i.get_pos().x + (tex.get_width() / 2 + 50)
		var ver_in = pos.y > i.get_pos().y - tex.get_height() / 2 and pos.y < i.get_pos().y + (tex.get_height() / 2 + 50)

		if  hor_in and ver_in:
			return i

	return null


func on_screen_drag(event):

	for i in gestures:

		if i.index == event.index:

			gestures[i].translate(Vector2(0, event.relative_y))
			gestures[i].released = false


func on_touch_released(index):

	for i in gestures:

		if i.index == index:

			gestures[i].released = true
			gestures.erase(i)


func damage_cow(damage):

	cow_hp -= damage
	get_node("CowHp").set_text(str(cow_hp))


class Gesture:

	var start = null
	var end = null
	var index = null
	var angle = null