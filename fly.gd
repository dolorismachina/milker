extends Node2D


var target_pos = Vector2(0, 0)

var damage_timer
var damage_timer_is_running = false


func _ready():

	damage_timer = get_node("DamageTimer")

	set_process_input(true)
	set_process(true)


func _process(delta):

	var dir = target_pos - get_pos()
	dir = dir.normalized()

	if not reached_target():
		translate(dir * 100 * delta)
	elif not damage_timer_is_running:
		damage_timer.start()
		damage_timer_is_running = true


func _input(event):

	if event.type == InputEvent.SCREEN_TOUCH or (event.type == InputEvent.MOUSE_BUTTON and event.button_index == 1):
		var tex_size = get_node("Sprite").get_texture().get_size() * get_scale()
		if event.pos.x > get_pos().x - tex_size.x / 2 and event.pos.x < get_pos().x + tex_size.x / 2 \
			and event.pos.y > get_pos().y - tex_size.y / 2 and event.pos.y < get_pos().y + tex_size.y / 2:
				queue_free()


func reached_target():

	return abs((get_pos() - target_pos).length()) < 10


func _on_DamageTimer_timeout():

	var n = get_tree().get_root().get_node("Node2D")
	n.damage_cow(1)

