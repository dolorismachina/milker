extends Node2D


export(PackedScene) var fly_scene

var spawn_locations = []


func _ready():

	randomize()

	spawn_locations.append(Vector2(0, 0))
	spawn_locations.append(Vector2(720, 0))
#	spawn_locations.append(Vector2(0, 640))
#	spawn_locations.append(Vector2(720, 640))


func _on_Timer_timeout():

	spawn_new_fly()


func spawn_new_fly():

	var rnd = randi() % 2
	var loc = spawn_locations[rnd]
	var fly = fly_scene.instance()
	fly.set_pos(loc)
	fly.target_pos = random_position()
	self.add_child(fly)


func random_position():

	var rand_x = randi() % int(get_viewport_rect().size.width)
	var rand_y = 560 + randi() % (850-560)

	return Vector2(rand_x, rand_y)