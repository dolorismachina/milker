extends Sprite

var released = true


func _ready():

	set_process(true)


func _process(delta):


	if get_pos().y > 1000:
		set_pos(Vector2(get_pos().x, 1000))
	if released and get_pos().y > 952:
		translate(Vector2(0, -500) * delta)
	elif get_pos().y < 952:
		set_pos(Vector2(get_pos().x, 952))
